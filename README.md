# GandynV5

## _a bash script to point a gandi domain on a dynamic ip_

### For who ?
This script is intended for people like me who have an old tiny computer running just below my box, and on which I test things. I want to access it through a domain I purchased on [Gandi.net](http://gandi.net/), but the box gives dynamic IPs. So this script is running periodically thanks to a CRON, and is updating the IP whenever it change.  
Or it's dedicated to people who simply wants to have an automatic updater of their dynamic IPs on Gandi's DNS zone RECORDS.

#### Install:
* Downdload [zip file](https://gitlab.com/sodimel/GandynV5/repository/archive.zip) via `wget https://gitlab.com/sodimel/GandynV5/repository/archive.zip`.
* Extract zip file.
* [*optional*] Remove `README.md` & `LICENSE`, or move `GandynV5.sh` to another place.

### Use:
* Modify `$APIKEY` (value `YOURAPIKEYHERE`) with the key you can found [here](https://account.gandi.net/fr/) (section "Security" > "API key").
* Modify `$DOMAIN` (value `YOURDOMAINURLHERE`) with your domain.
* Execute the script via `./GandynV5.sh` (make sure you have the rights to do that. If no, run `chmod +x GandynV5.sh`).
* And its done.
* [*optional*] You can add `*/30 * * * * /path/to/GandynV5.sh` to your cron via `crontab -e` to makes the computer run the script every half an hour.
* [*optional*] Instead of using cron, you can also install the systemd service and timer files in the directory `~/.config/systemd/user` (create it if it does not exist) and then enable the timer with: `systemctl --user enable GandynV5.timer && systemctl --user start GandynV5.timer`.

### Prerequisites:
* The script uses `curl`, `dig` (from package `dnsutils`), and `host` (from package `bind9-host`).