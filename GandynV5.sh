#/bin/bash

# #####
# Author: Corentin Bettiol
# Version: 2
# Sources: https://gitlab.com/sodimel/gandynV5/
#
# Realized with the help of http://doc.livedns.gandi.net/ :)
# #####

# #####
# Variables
# #####

APIKEY='YOURAPIKEYHERE'
DOMAIN='YOURDOMAINURLHERE'
DEBUG=False

# #####
# Script
# #####

newIp=`dig +short myip.opendns.com @resolver1.opendns.com`
oldIp=`host $DOMAIN | grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}'`

if [ "$newIp" != "$oldIp" ]; # if the newIp is different
then

	zoneDNS=`curl -H "X-Api-Key: $APIKEY" \
		https://dns.api.gandi.net/api/v5/domains/$DOMAIN/records`
	
	zoneDNS="${zoneDNS/$oldIp/$newIp}" # replace the first occurence of $oldIp by $newIp

	if [ "$DEBUG" == "True" ]; # echo of the new dns zone that will be sent to the gandi api
	then
		echo "New Ip:" $newIp
		echo "Old Ip:" $oldIp
		echo "new zoneDNS data:"
		echo $zoneDNS
	fi

	curl -X PUT -H "Content-Type: application/json" \
		-H "X-Api-Key: $APIKEY" \
		-d "{\"items\": $zoneDNS}" \
		https://dns.api.gandi.net/api/v5/domains/$DOMAIN/records
fi